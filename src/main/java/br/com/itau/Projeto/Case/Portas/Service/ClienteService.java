package br.com.itau.Projeto.Case.Portas.Service;


import br.com.itau.Projeto.Case.Portas.Exceptions.ClienteNaoEncontrado;
import br.com.itau.Projeto.Case.Portas.Model.Cliente;
import br.com.itau.Projeto.Case.Portas.Repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente CriarNovoCliente(Cliente cliente) {

        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorId(int id) {

        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            return cliente;
        } else {
            throw new ClienteNaoEncontrado();
        }

    }
}

