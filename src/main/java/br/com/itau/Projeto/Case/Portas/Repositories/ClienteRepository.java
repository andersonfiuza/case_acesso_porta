package br.com.itau.Projeto.Case.Portas.Repositories;

import br.com.itau.Projeto.Case.Portas.Model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository  extends CrudRepository<Cliente, Integer> {
}
