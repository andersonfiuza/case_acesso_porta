package br.com.itau.Projeto.Case.Portas.Controller;


import br.com.itau.Projeto.Case.Portas.Model.Cliente;
import br.com.itau.Projeto.Case.Portas.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
//@RequestMapping
public class ClienteController {

@Autowired
    ClienteService clienteService;

@PostMapping
    public Cliente criarNovoCLiente(@RequestBody @Valid Cliente cliente) {

        return  clienteService.CriarNovoCliente(cliente);

    }
    @GetMapping(value = "/{id}")
    public Cliente buscarClienteId(@PathVariable(name = "id") int id){
       return clienteService.buscarClientePorId(id);


    }


}
